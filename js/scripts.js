//слайдеры главная страница
$('.promo__slider').slick({
	dots: false,
	slidesToShow: 1,
	slidesToScroll: 1,
	infinite: true,
	speed: 1000,
	arrows: true,
	appendArrows: $('.promo__slider-nav-wrapper'),
	prevArrow: '<div class="promo__slider-nav promo__slider-nav_left"></div>',
	nextArrow: '<div class="promo__slider-nav promo__slider-nav_right"></div>'
});

$('.works__slider').slick({
	dots: false,
	slidesToShow: 1,
	slidesToScroll: 1,
	infinite: true,
	speed:1000,
	arrows: true,
	prevArrow: '<div class="works__slider-nav works__slider-nav_left"></div>',
	nextArrow: '<div class="works__slider-nav works__slider-nav_right"></div>',
	responsive: [
		{
			breakpoint: 760,
			settings: "unslick"
		},
	]
})

$('.partners__slider').slick({
	dots: false,
	slidesToShow: 5,
	slidesToScroll: 1,
	infinite: true,
	arrows: true,
	prevArrow: '<div class="partners__slider-nav partners__slider-nav_left"></div>',
	nextArrow: '<div class="partners__slider-nav partners__slider-nav_right"></div>'
})

$('.about__slider').slick({
	dots: false,
	slidesToShow: 1,
	slidesToScroll: 1,
	infinite: true,
	speed: 1000,
	arrows: true,
	prevArrow: '<div class="about__slider-nav about__slider-nav_left"></div>',
	nextArrow: '<div class="about__slider-nav about__slider-nav_right"></div>',
});

$('.team__slider-inner').slick({
	dots: false,
	slidesToShow: 3,
	slidesToScroll: 1,
	infinite: true,
	speed: 1000,
	arrows: true,
	prevArrow: '<div class="team__slider-nav team__slider-nav_left"></div>',
	nextArrow: '<div class="team__slider-nav team__slider-nav_right"></div>',
	responsive: [
		{
		  breakpoint: 1023,
		  settings: {
			slidesToShow: 2
		  }
		},
		{
		  breakpoint: 759,
		  settings: {
			slidesToShow: 1
		  }
		}
	]
});


$('.workers__slider').slick({
	dots: false,
	slidesToShow: 1,
	slidesToScroll: 1,
	infinite: true,
	speed: 1000,
	arrows: true,
	prevArrow: '<div class="workers__slider-nav workers__slider-nav_left"></div>',
	nextArrow: '<div class="workers__slider-nav workers__slider-nav_right"></div>'
});


$('.services-works__slider').slick({
	dots: false,
	slidesToShow: 4,
	slidesToScroll: 1,
	infinite: true,
	speed: 1000,
	arrows: true,
	prevArrow: '<div class="services-works__slider-nav services-works__slider-nav_left"></div>',
	nextArrow: '<div class="services-works__slider-nav services-works__slider-nav_right"></div>',
	responsive: [
		{
		  breakpoint: 1270,
		  settings: {
			slidesToShow: 3
		  }
		},
		{
		  breakpoint: 760,
		  settings: {
			slidesToShow: 1,
			variableWidth: true,
		  }
		}
	]
});


$('.stuff__slider').slick({
	dots: false,
	slidesToShow: 1,
	slidesToScroll: 1,
	infinite: true,
	speed: 1000,
	arrows: true,
	prevArrow: '<div class="stuff__slider-nav stuff__slider-nav_left"></div>',
	nextArrow: '<div class="stuff__slider-nav stuff__slider-nav_right"></div>'
});



// Инициализация Карты
$(function(){
	var oYMap = {
		init: function(){
			if($('#js-map').length){
				ymaps.ready(oYMap.initMap);
			}
		},
		initMap: function(){
			var jMap = $('#js-map'),
				aCenter = jMap.data('center') ? jMap.data('center') : [53.285738, 34.334617];

			oYMap.yMap = new ymaps.Map('js-map', {
				center: aCenter,
				zoom: 11
			}, {});

			var placemark1 = new ymaps.Placemark([53.309844, 34.305771], {
					hintContent: '',
					balloonContent: 'Брянск ул. Комсомольская, 12 '
				}, {
					iconLayout: 'default#image',
					iconImageHref: '/f/i/svg/icon_geo.svg',
					iconImageSize: [26, 35],
					iconImageOffset: [-13, -35]
				});

			var placemark2 = new ymaps.Placemark([53.262461, 34.413470], {
					hintContent: '',
					balloonContent: 'Брянск ул. Никитина, 5 '
				}, {
					iconLayout: 'default#image',
					iconImageHref: '/f/i/svg/icon_geo.svg',
					iconImageSize: [26, 35],
					iconImageOffset: [-13, -35]
				});

			var placemark3 = new ymaps.Placemark([53.235737, 34.351424], {
					hintContent: '',
					balloonContent: 'Брянск ул. Красноармейская, 39 '
				}, {
					iconLayout: 'default#image',
					iconImageHref: '/f/i/svg/icon_geo.svg',
					iconImageSize: [26, 35],
					iconImageOffset: [-13, -35]
				});

			//Выключим зумм скроллом
			oYMap.yMap.behaviors.disable('scrollZoom');

			// Добавим наши метки на карту
			oYMap.yMap.geoObjects.add(placemark1);
			oYMap.yMap.geoObjects.add(placemark2);
			oYMap.yMap.geoObjects.add(placemark3);
		}
	}

	oYMap.init();
});



//popup
var Popup = {
	init: function () {
		$(document).on('click', '[data-popup-open]', Popup.openSelector);
		$(document).on('click', '[data-popup-close]', Popup.closeSelector);
	},
	openSelector: function (e) {
		var jThis = $(e.currentTarget),
			sPopUpId = jThis.attr('data-popup-open');
		e.preventDefault();
		Popup.open(sPopUpId);
	},
	open: function (sPopUpId) {
		var jPopUp = $('[data-popup-id="' + sPopUpId + '"]');
		jPopUp.stop().fadeIn()
		return false;
	},
	closeSelector: function (e) {
		var jThis = $(e.currentTarget)
		sPopUpId = jThis.attr('data-popup-close');
		e.preventDefault();
		Popup.close(sPopUpId);
	},
	close: function (sPopUpId) {
		var jPopUp = $('[data-popup-id="' + sPopUpId + '"]');
		jPopUp.fadeOut()
		return false;
	}
};
$(function () {
  Popup.init();
})

//Поисковик
var OpenSearch = {
	init: function () {
		$(document).on('click', '.js__open-search', OpenSearch.open)
	},
	open: function(event) {
		var input = $(event.currentTarget)
		$('.js__search-input').fadeIn()
		$(document).on('click', '', OpenSearch.close)
	},
	close: function(event) {
		if ($(event.target).is('.js__search-input *')) {
			return
		}
		$('.js__search-input').fadeOut()
		$(document).off('click', OpenSearch.close)
	}
};

$(function () {
	OpenSearch.init();
})

//custom scroll
$('.b-policy__content').customScroll();



//cелекторы формы
var OpenSelect = {
	init: function () {
		$(document).on('click', '.js__input-main', OpenSelect.open)
		$(document).on('click', '.js__input-select', OpenSelect.chooseData)
	},
	open: function (event) {
		var input = $(event.currentTarget)
		var list = input.closest('.js__block-select')
		list.find('.js__list-select').slideDown()
		$(document).on('click', '', OpenSelect.close)
	},
	close: function(event) {
		if ($(event.target).is('.js__input-main, .js__list-select *')) {
			return
		}
		$('.js__list-select').slideUp()
		$(document).off('click', OpenSelect.close)
	},
	chooseData: function(event){
		var data = $(event.currentTarget)
		var chooseData = $(event.currentTarget).html()
		var list = data.closest('.js__block-select')
		list.find('.js__input-main').html(chooseData)
		list.find('.js__input-hidden').val(chooseData)
		$('.js__list-select').slideUp()
	}
};

$(function () {
	OpenSelect.init();
})


//datepicker init
$(function () {
	$('.b-form__input_date').datepicker({
		range: true,
		timepicker: true,
		startHours: 10,
		minHours: 10,
		maxHours: 19,
		autoClose: true,
		multipleDatesSeparator: ' - ',
		dateFormat: 'dd.M'
	});

	$('.b-form__input_date').data('datepicker').$datepicker.insertAfter('.b-form__input_date')
})

//
var RequestForm = {
	init: function(){
		$(document).on('click', '[data-clone-id]', this.onClone.bind(this))
	},
	onClone: function(event){
		var btn = $(event.currentTarget)
		var elementId = btn.data('clone-id')

		this.cloneElement(elementId)
	},
	cloneElement: function(elementId){
		var element = $('[data-clone-element="' + elementId + '"]')
		var newElement = element.clone()
		newElement.removeAttr('data-clone-element')
		newElement.insertAfter(element)
	}

}

$(function () {
	RequestForm.init();
})


var WorkSlider = {
	init: function () {
		var sliders = $('.works__slider')
		if (screen.width < 760 && sliders.length > 0) {
			sliders.each(function (index, slider) {
				WorkSlider.parseElements(slider)
			})
		}
	},
	parseElements: function (slider) {
		var items = $(slider).find('.js__slider-item')
		var list = $(slider)

		list.empty().append(items)
		WorkSlider.initSlider(slider)
	},
	initSlider: function (slider) {
		$(slider).slick({
			dots: false,
			variableWidth: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			infinite: true,
			speed: 1000,
			arrows: true,
			appendArrows: $('.works__subscribe'),
			prevArrow: '<div class="works__slider-nav works__slider-nav_left"></div>',
			nextArrow: '<div class="works__slider-nav works__slider-nav_right"></div>',
			});
	}
};


$(function () {
	WorkSlider.init();
})
console.log('123');

var Menu = {
	init: function () {
		$(document).on('touchstart', '.js__mobile-btn', this.onToggle)
		$(document).on('touchstart', '.js__mobile-close', this.close)
	},
	onToggle: function (event) {
		var btn = $(event.currentTarget)
		var menu = $('.is-mobile-menu')

		if (menu.hasClass('is-opened')) {
			Menu.close()
		} else {
			Menu.open()
		}
	},
	open: function () {
		var menu = $('.is-mobile-menu')
		menu.addClass('is-opened')
	},
	close: function () {
		var menu = $('.is-mobile-menu')
		menu.removeClass('is-opened')
	},
}

$(function () {
	Menu.init();
})
