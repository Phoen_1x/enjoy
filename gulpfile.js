var gulp = require('gulp');
var stylus = require('gulp-stylus');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var nib = require('nib');
var browserSync = require('browser-sync').create();

var buildCss = function () {
	  console.log('buildcss');
	  gulp.src('./src/css/styles.styl')
	  .pipe(plumber({
            errorHandler: notify.onError(function(err){
                return {
                    title: 'Ошибка (css)',
                    message: err.message
                }
            })
     	}))	
	  .pipe(stylus({
	  	use:[nib()]
	  }))
	  .pipe(gulp.dest('./css/'))
	  .pipe(browserSync.stream());
} 

gulp.task('css', buildCss);

gulp.task('serve', ['css'], function() {

    browserSync.init({
        server: ""
    });

    gulp.watch(["./src/css/*.*", "./src/css/*/*.*"], ['css']);
    gulp.watch("./src/*.html").on('change', browserSync.reload);
});


gulp.task('default', ['serve']);